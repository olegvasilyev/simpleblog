﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleBlog.Models;
using SimpleBlog.BLL;
using Microsoft.Extensions.Options;

namespace SimpleBlog.Controllers
{
    public class HomeController : BaseController
    {
        //_blogAccessor is used to get option settings from an appsettings.json from the section "BlogConfiguration".
        private readonly IOptions<BlogSettings> _blogAccessor;

        public HomeController(IPostManager mng, IOptions<BlogSettings> blogAccessor)
            : base(mng)
        {
            _blogAccessor = blogAccessor;
        }

        public IActionResult Index()
        {
            var vm = mng.GetTags();
            return View(vm);
        }

        //Method to get posts by ajax request with parameters mapped to BlogPostOptions class.
        [HttpPost]
        public IActionResult GetPosts(BlogPostOptions parameters)
        {
            var res = false;
            PostListViewModel items = null;
            if (ModelState.IsValid)
            {
                var options = new BlogPostOptions();

                options.Page = parameters.Page != 0 ? parameters.Page : _blogAccessor.Value.Page;
                options.PageSize = parameters.PageSize != 0 ? parameters.PageSize : _blogAccessor.Value.PageSize;
                options.Search = String.IsNullOrEmpty(parameters.Search) ? "" : parameters.Search;
                options.TagID = parameters.TagID != 0 ? parameters.TagID : 0;

                items = mng.GetBlogPosts(options);

                res = items != null && items.Posts.Count > 0 ? true : false;
            }
            return Json(new
            {
                result = res,
                data = items
            });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //Method to save replay to post ot another user's comment.
        [HttpPost]
        public IActionResult CreateComment(NewCommentViewModel model)
        {
            var res = false;
            var msg = "";
            if (ModelState.IsValid)
            {
                var item = mng.CreateCommentAsync(model).Result;
                if (item.Id > 0)
                {
                    msg = "Your comment was saved successfully";
                    res = true;
                }
            }
            return Json(new
            {
                result = res,
                msg
            });
        }
    }
}
