﻿using Microsoft.AspNetCore.Mvc;
using SimpleBlog.BLL;
using System.Collections.Generic;

namespace SimpleBlog.Controllers
{
    public class BaseController : Controller
    {

        protected IPostManager mng;


        public BaseController(IPostManager mng)
        {
            this.mng = mng;

        }
        protected override void Dispose(bool disposing)
        {
            if (mng != null) mng.Dispose();
        }

    }
}