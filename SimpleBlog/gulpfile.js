﻿/// <binding ProjectOpened='watch-js, watch-sass' />
//Minification works through bundleconfig engine, which is configured by bundleconfig.json file

var gulp = require('gulp'),
    rimraf = require("gulp-rimraf"),
    concat = require("gulp-concat"),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    clean = require('gulp-clean');

var paths = {
    webroot: "./wwwroot/"
};

paths.jsDest = paths.webroot + 'js/';
paths.js = paths.webroot + "src/js/*.js";
paths.adminJs = paths.webroot + "src/js/admin/*.js";
paths.cleanJs = paths.webroot + "js/**/*.js";
paths.watchJs = paths.webroot + "src/js/**/*.js";

paths.cssDest = paths.webroot + "css/";
paths.siteScss = paths.webroot + "src/scss/style.scss";
paths.adminScss = paths.webroot + "src/scss/admin/style.scss";
paths.cleanCss = paths.webroot + "css/**/*.css";
paths.watchSass = paths.webroot + "src/scss/**/*.scss";


gulp.task("process-js", function () {
    var options = {
        destPath: paths.jsDest,
        chanks: [
            {
                sourceFiles: paths.js,
                outputFile: 'site.js'
            },
            {
                sourceFiles: paths.adminJs,
                outputFile: 'admin.js'
            }
        ]
    };

    for (var i in options.chanks) {
        var chank = options.chanks[i];

        gulp.src(chank.sourceFiles)
            .pipe(concat(chank.outputFile))
            .pipe(gulp.dest(options.destPath))
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(uglify())
            .pipe(gulp.dest(options.destPath));
    }
});

gulp.task('clean-js', function (cb) {
    gulp.src(paths.cleanJs).pipe(clean()).on('end', function () {
        console.log("clean-js end");
        cb();
    });
});

gulp.task("process-css", function () {
    var options = {
        destPath: paths.cssDest,
        chanks: [
            {
                sourceFiles: paths.siteScss,
                outputFile: 'site.css'
            },
            {
                sourceFiles: paths.adminScss,
                outputFile: 'admin.css'
            }
        ]
    };

    for (var i in options.chanks) {
        var chank = options.chanks[i];

        gulp.src(chank.sourceFiles)
            .pipe(sass())
            .pipe(rename(chank.outputFile))
            .pipe(gulp.dest(options.destPath))
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(autoprefixer({
                browsers: ['last 50 versions']
            }))
            .pipe(cssmin())
            .pipe(gulp.dest(options.destPath));
    }
});

gulp.task('clean-css', function (cb) {
    gulp.src(paths.cleanCss).pipe(clean()).on('end', function () {
        console.log("clean-css end");
        cb();
    });
});

gulp.task('clean-all', ['clean-js', 'clean-css']);

gulp.task("watch-sass", function () {
    gulp.watch(paths.watchSass, ["process-css"]);
});

gulp.task("watch-js", function () {
    gulp.watch(paths.watchJs, ["process-js"]);
});
