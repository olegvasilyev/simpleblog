//Show popup alert
function ShowBlogAlert(msg, isReload, time) {

    if (!isReload) {
        isReload = false;
    }

    if (!time) {
        time = 3000;
    }

    var dialog = bootbox.dialog({
        message: msg,
    });

    setTimeout(function () {
        dialog.modal('hide');
        if (isReload) {
            location.reload();
        }
    }, time);
};
var blog = blog || {};

//blog.comments is a component to manage comments in cabinet. Here are event handlers and functions to to create, render and manipulate comments
blog.comments = {
    options: {
        postID: 0
    },

    init: function () {
        blog.comments.options.postID = currentPostID;

        blog.comments.getComments();

        $document = $(document);
        //Event handler section
        $document.on('click', '.edit-comment-visible', function (e) {
            e.preventDefault();

            var isVisible = $(e.currentTarget).data('isvisible');
            var commentID = $(e.currentTarget).data('id');

            if (isVisible) {
                bootbox.confirm("All comments for this comment will be invisible as well", function (result) {
                    if (!result) {
                        return;
                    }
                    else {
                        $.ajax({
                            method: "POST",
                            url: "/Admin/Home/ChangeVisibleStatus",
                            data: { commentID: commentID, isVisible: !isVisible }
                        })
                            .done(function (data) {
                                if (data.result) {
                                    ShowBlogAlert(data.msg);
                                    blog.comments.getComments();
                                }
                                else {
                                    ShowBlogAlert(data.msg);
                                }
                            });
                    }
                });
            }
            else {
                $.ajax({
                    method: "POST",
                    url: "/Admin/Home/ChangeVisibleStatus",
                    data: { commentID: commentID, isVisible: !isVisible }
                })
                    .done(function (data) {
                        if (data.result) {
                            ShowBlogAlert(data.msg);
                            blog.comments.getComments();
                        }
                        else {
                            ShowBlogAlert(data.msg);
                        }
                    });
            }
        });

        $document.on('click', '.edit-comment-replay', function (e) {
            e.preventDefault();

            var id = $(e.currentTarget).data('id');
            var postID = $(e.currentTarget).data('postid');

            if (!$('#replaty-wrapper-' + id).is(':empty')) {
                $('#replaty-wrapper-' + id).empty();
                return;
            }

            var template = $.templates("#create-replay-template");
            var htmlOutput = template.render({ id: id, postID: postID });

            $('#replaty-wrapper-' + id).append(htmlOutput);
        });

        $document.on('click', '.replay-cancel', function (e) {
            e.preventDefault();

            var id = $(e.currentTarget).data('id');

            if (!$('#replaty-wrapper-' + id).is(':empty')) {
                $('#replaty-wrapper-' + id).empty();
                return;
            }

        });

        $document.on('click', '.replay-submit', function (e) {
            e.preventDefault();

            var commenID = $(e.currentTarget).data('id');
            var postID = $(e.currentTarget).data('postid');
            var text = $('#create-replay-text-' + commenID).val();

            if (!text || text === "") {
                ShowBlogAlert("Enter comment text");
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/CreateCommentAsync",
                data: { PostID: postID, CommentID: commenID, Text: text }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        blog.comments.getComments();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '.edit-comment-delete', function (e) {
            e.preventDefault();

            var commentID = $(e.currentTarget).data('id');

            bootbox.confirm("All child comments for this comment will be deleted as well", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeleteComment",
                        data: { commentID: commentID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg, true);
                                blog.comments.getComments();
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });
    },

    //Getting comments by ajax request and render response result
    getComments: function () {
        $.ajax({
            method: "POST",
            url: "/Admin/Home/GetComments",
            data: { postID: blog.comments.options.postID }
        })
            .done(function (data) {
                if (data.result) {
                    blog.comments.render(data);
                }
                else {
                    ShowBlogAlert("There are no data or data server unaccesible");
                }
            });
    },

    render: function (data) {
        $('.comments-wrapper').empty();
        var postTemplate = $.templates('#post-comment-template');
        $.each(data.posts, function (key, item) {
            var htmlOutput = postTemplate.render(item);
            $('.comments-wrapper').append(htmlOutput);
        });

        var commentTemplate = $.templates('#comment-template');
        $.each(data.items, function (key, item) {
            if (item.commentID === 0) {
                var htmlOutput = commentTemplate.render(item);
                $('#post-comments-wrapper-' + item.postID).prepend(htmlOutput);
            }
            else if (item.commentID > 0) {
                blog.comments.renderChild(item);
            }
        });
    },

    renderChild: function (item) {
        var commentTemplate = $.templates("#comment-template");
        var htmlOutput = commentTemplate.render(item);
        $('#child-comments-wrapper-' + item.commentID).prepend(htmlOutput);
    }
};
var blog = blog || {};

//blog.editPost is a component to edit or create post
blog.editPost = {
    options: {
        isNewPost: true
    },
    init: function () {
        blog.editPost.options.isNewPost = currentPostID === 0 ? true : false;

        $("#edit-post-form").submit(function (e) {
            e.preventDefault();

            var title = $('#edit-post-title').val();
            var description = $('#edit-post-descryption').val();
            var text = $('#edit-post-text').val();
            var isPublished = $('#edit-post-is-published').is(":checked");

            $("span[class*='validation-info']").addClass('invisible');

            if (!title || title === "") {
                $('#edit-post-title').focus();
                if ($('.title-validation-info').hasClass('invisible')) {
                    $('.title-validation-info').removeClass('invisible');
                }
                return;
            }

            if (!description || description === "") {
                $('#edit-post-descryption').focus();
                if ($('.descryption-validation-info').hasClass('invisible')) {
                    $('.descryption-validation-info').removeClass('invisible');
                }
                return;
            }

            if (!text || text === "") {
                $('#edit-post-text').focus();
                if ($('.text-validation-info').hasClass('invisible')) {
                    $('.text-validation-info').removeClass('invisible');
                }
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/EditPostAsync",
                data: { Id: currentPostID, Title: title, Description: description, Text: text, IsPublished: isPublished }
            })
                .done(function (data) {
                    if (data.result) {
                        window.location = "/admin/Home/EditPost/" + data.postID;
                    }
                });
        });

        $document = $(document);

        $document.on('click', '#delete-post-button', function (e) {
            e.preventDefault();

            var postID = $('#delete-post-button').data('postid');
            if (!postID || postID === 0) {
                return;
            }

            bootbox.confirm("All comments for this post will be deleted with post", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeletePost",
                        data: { postID: postID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg, true);
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });

        $document.on('change', 'input[type="checkbox"].is-checked', function (e) {
            e.preventDefault();

            if (blog.editPost.options.isNewPost) {
                ShowBlogAlert('You can not set or unset tag to unsaved post. Please save post before');
                return;
            }

            var element = $(e.currentTarget);
            var tagID = element.data('tagid');
            var isChecked = element.is(":checked");

            $.ajax({
                method: "POST",
                url: "/Admin/Home/SetTagToPost",
                data: { postID: currentPostID, tagID: tagID, isChecked: isChecked }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '#new-tag-submit', function (e) {
            e.preventDefault();

            var name = $('#new-tag-name').val();

            if (!name || name === "") {
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/CreateTag",
                data: { name: name }
            })
                .done(function (data) {
                    if (data.result) {
                        $('#new-tag-name').val('');
                        ShowBlogAlert(data.msg);
                        var template = $.templates("#tag-template");
                        var htmlOutput = template.render(data);
                        $('.tags-wrapper').append(htmlOutput);
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '.tag-group i.close-btn', function (e) {
            e.preventDefault();

            var element = $(e.currentTarget);
            var tagID = element.data('tagid');

            if (!tagID || tagID === "") {
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/DeleteTag",
                data: { tagID: tagID }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        element.closest('.tag-group').remove();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });
    }
};


var blog = blog || {};

//blog.postlist is a simple component to manage post list which is rendered in datatable
blog.postlist = {
    init: function () {
        $document = $(document);

        $document.on('click', 'button.delete-post', function (e) {
            e.preventDefault();

            var postID = $(e.currentTarget).data('postid');

            if (!postID || postID === "") {
                return;
            }

            bootbox.confirm("All comments for this post will be deleted with post", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeletePost",
                        data: { postID: postID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg);
                                postTable.ajax.reload();
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });

        $document.on('change', '.published-status', function (e) {
            e.preventDefault();

            var element = $(e.currentTarget);
            var postID = element.data('postid');
            var isPublished = element.is(":checked");

            $.ajax({
                method: "POST",
                url: "/Admin/Home/SetPublishedStatus",
                data: { postID: postID, isPublished: isPublished }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        postTable.ajax.reload();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });
    }
};