var blog = blog || {};

//blog.post component is an object, that contains event handlers and functions to manage post list
blog.post = {
    options: {
        parameters: { page: 1, pageSize: 10, tagID: 0, search: "" },
        newComment: {},
        $pagination: null,
        defaultOpts: {
            startPage: 1,
            totalPages: 5,
            visiblePages: 10,
            first: '<<',
            prev: '<',
            next: '>',
            last: '>>',
            onPageClick: function (event, page) {
                if (blog.post.options.parameters.page === page) {
                    return;
                }
                blog.post.options.parameters.page = page;
                blog.post.options.defaultOpts.startPage = page;
                blog.post.getPosts();
            }
        },
    },

    init: function () {
        $('#search-text-form').removeClass('hiddenElement');

        blog.post.options.parameters.page = defaultPage;
        blog.post.options.parameters.pageSize = defaultPageSize;

        blog.post.options.$pagination = $('#page-selection');
        blog.post.options.defaultOpts.startPage = defaultPage;
        blog.post.options.defaultOpts.visiblePages = defaultMaxVisible;

        blog.post.getPosts();

        //Handler's section
        $('.post-body').collapse('toggle');

        $document = $(document);

        $document.on('click', '#reset-blog-with-reload', function (e) {
            e.preventDefault();

            blog.post.options.parameters.page = defaultPage;
            blog.post.options.parameters.pageSize = defaultPageSize;
            blog.post.options.parameters.tagID = 0;
            blog.post.options.parameters.search = "";
            $('#search-text-input').val('');
            $('div.no-data-wrapper').addClass('hiddenElement');

            blog.post.options.defaultOpts.startPage = defaultPage;
            blog.post.options.defaultOpts.visiblePages = defaultMaxVisible;

            blog.post.getPosts();

        });

        $document.on('click', 'a.replay-control', function (e) {
            e.preventDefault();

            var id = $(e.currentTarget).data('id');
            var postID = $(e.currentTarget).data('postid');
            var itemID = $(e.currentTarget).data('itemid');
            var replaytTo = $(e.currentTarget).data('author');

            if (!$('#replay-wrapper-' + postID + itemID).is(':empty')) {
                $('#replay-wrapper-' + postID + itemID).empty();
                return;
            }

            $('.replay-wrapper').empty();

            var replayTemplate = $.templates('#replay-template');
            var htmlOutput = replayTemplate.render({ postID: postID, commentID: itemID, replaytTo: replaytTo });

            $('#replay-wrapper-' + postID + itemID).append(htmlOutput);
        });

        $document.on('click', '.post-tag-item', function (e) {
            e.preventDefault();

            var tagID = $(e.currentTarget).data('tagid');

            if (blog.post.options.parameters.tagID === tagID) {
                return;
            } else {
                blog.post.options.parameters.tagID = tagID;
                blog.post.options.parameters.page = defaultPage;
                blog.post.options.defaultOpts.startPage = defaultPage;
            }
            blog.post.getPosts();
        });

        $document.on('submit', '#replay-comment-form', function (e) {
            e.preventDefault();

            $('.validation-error').css('opacity', '0');

            var userName = $('#edit-replay-userName').val();
            var email = $('#edit-replay-email').val();
            var text = $('#edit-replay-text').val();
            var postID = $(e.currentTarget).data('postid');
            var commentID = $(e.currentTarget).data('commentid');

            if (!userName || userName === "") {
                $('.username-validation').css('opacity', '1');
                $('#edit-replay-userName').focus();
                return;
            }

            if (!email || email === "") {
                $('.email-validation').css('opacity', '1');
                $('#edit-replay-email').focus();
                return;
            }

            if (!text || text === "") {
                $('.text-validation').css('opacity', '1');
                $('#edit-replay-text').focus();
                return;
            }

            blog.post.options.newComment.userName = userName;
            blog.post.options.newComment.email = email;
            blog.post.options.newComment.text = text;
            blog.post.options.newComment.postID = postID;
            blog.post.options.newComment.commentID = commentID;

            $('#confirm-comment-modal').modal('show');

            return;
        });

        $document.on('submit', '#search-text-form', function (e) {
            e.preventDefault();

            var text = $('#search-text-input').val();

            if (blog.post.options.parameters.search !== text) {
                blog.post.options.parameters.search = text;
            }

            blog.post.getPosts();

            return;
        });

        $document.on('click', '#clear-search', function (e) {
            e.preventDefault();

            blog.post.options.parameters.search = "";
            $('#search-text-input').val('');
        });

        $document.on('click', 'a.collapse-control', function (e) {
            e.preventDefault();

            var element = $(e.currentTarget);

            if (element.text() === 'Read post') {
                element.text('Collapse post');
            }
            else {
                element.text('Read post');
            }

            var postID = $(e.currentTarget).data('postid');
            $('#post-body-' + postID).collapse('toggle');
        });

    },

    //Main function to get post by ajax request and call render method to show post and comments
    getPosts: function () {
        $.ajax({
            method: "POST",
            url: "/Home/GetPosts",
            data: blog.post.options.parameters
        })
            .done(function (response) {
                if (response.result) {
                    if (!$('div.no-data-wrapper').hasClass('hiddenElement')) {
                        $('div.no-data-wrapper').addClass('hiddenElement');
                    }
                    blog.post.options.defaultOpts.totalPages = response.data.pages;
                    blog.post.options.$pagination.twbsPagination('destroy');
                    blog.post.options.$pagination.twbsPagination(blog.post.options.defaultOpts);
                }
                else {
                    blog.post.options.defaultOpts.totalPages = response.data.pages;
                    blog.post.options.$pagination.twbsPagination('destroy');
                    $('div.no-data-wrapper').removeClass('hiddenElement');
                }
                $('.post-tag-item').removeClass('selected-tag');
                $('.post-tag-item[data-tagID="' + response.data.tagID + '"]').addClass('selected-tag');
                blog.post.render(response.data);
            });
    },

    render: function (data) {
        $('#blog-list-wrapper').empty();
        var postTemplate = $.templates('#post-template');
        var commentTemplate = $.templates('#comment-template');

        $.each(data.posts, function (key, item) {
            var htmlOutput = postTemplate.render(item);
            $('#blog-list-wrapper').append(htmlOutput);

            $.each(item.comments, function (key, item) {
                if (item.commentID === 0) {
                    var htmlOutput = commentTemplate.render(item);
                    $('#post-comments-wrapper-' + item.postID).prepend(htmlOutput);
                }
                else if (item.commentID > 0) {
                    blog.post.renderChild(item);
                }
            });

        });
    },

    renderChild: function (item) {
        var commentTemplate = $.templates("#comment-template");
        var htmlOutput = commentTemplate.render(item);
        $('#child-comments-wrapper-' + item.commentID).prepend(htmlOutput);
    },

    saveNewComment: function () {
        $.ajax({
            method: "POST",
            url: "/Home/CreateComment",
            data: blog.post.options.newComment
        })
            .done(function (data) {
                if (data.result) {
                    blog.post.showBlogAlert(data.msg);
                    blog.post.getPosts();
                }
                else {
                    ShowBlogAlert(data.msg);
                }

                blog.post.options.newComment = {};
            });
    },

    //verifyCallback is a function that process reCAPTCHA response
    verifyCallback: function (response) {
        $('#confirm-comment-modal').modal('hide');
        grecaptcha.reset();

        if (response) {
            blog.post.saveNewComment();
        }
    },

    showBlogAlert: function (msg, isReload, time) {

        if (!isReload) {
            isReload = false;
        }

        if (!time) {
            time = 3000;
        }

        var dialog = bootbox.dialog({
            message: msg,
        });

        setTimeout(function () {
            dialog.modal('hide');
            if (isReload) {
                location.reload();
            }
        }, time);
    }
};