﻿var blog = blog || {};

//blog.comments is a component to manage comments in cabinet. Here are event handlers and functions to to create, render and manipulate comments
blog.comments = {
    options: {
        postID: 0
    },

    init: function () {
        blog.comments.options.postID = currentPostID;

        blog.comments.getComments();

        $document = $(document);
        //Event handler section
        $document.on('click', '.edit-comment-visible', function (e) {
            e.preventDefault();

            var isVisible = $(e.currentTarget).data('isvisible');
            var commentID = $(e.currentTarget).data('id');

            if (isVisible) {
                bootbox.confirm("All comments for this comment will be invisible as well", function (result) {
                    if (!result) {
                        return;
                    }
                    else {
                        $.ajax({
                            method: "POST",
                            url: "/Admin/Home/ChangeVisibleStatus",
                            data: { commentID: commentID, isVisible: !isVisible }
                        })
                            .done(function (data) {
                                if (data.result) {
                                    ShowBlogAlert(data.msg);
                                    blog.comments.getComments();
                                }
                                else {
                                    ShowBlogAlert(data.msg);
                                }
                            });
                    }
                });
            }
            else {
                $.ajax({
                    method: "POST",
                    url: "/Admin/Home/ChangeVisibleStatus",
                    data: { commentID: commentID, isVisible: !isVisible }
                })
                    .done(function (data) {
                        if (data.result) {
                            ShowBlogAlert(data.msg);
                            blog.comments.getComments();
                        }
                        else {
                            ShowBlogAlert(data.msg);
                        }
                    });
            }
        });

        $document.on('click', '.edit-comment-replay', function (e) {
            e.preventDefault();

            var id = $(e.currentTarget).data('id');
            var postID = $(e.currentTarget).data('postid');

            if (!$('#replaty-wrapper-' + id).is(':empty')) {
                $('#replaty-wrapper-' + id).empty();
                return;
            }

            var template = $.templates("#create-replay-template");
            var htmlOutput = template.render({ id: id, postID: postID });

            $('#replaty-wrapper-' + id).append(htmlOutput);
        });

        $document.on('click', '.replay-cancel', function (e) {
            e.preventDefault();

            var id = $(e.currentTarget).data('id');

            if (!$('#replaty-wrapper-' + id).is(':empty')) {
                $('#replaty-wrapper-' + id).empty();
                return;
            }

        });

        $document.on('click', '.replay-submit', function (e) {
            e.preventDefault();

            var commenID = $(e.currentTarget).data('id');
            var postID = $(e.currentTarget).data('postid');
            var text = $('#create-replay-text-' + commenID).val();

            if (!text || text === "") {
                ShowBlogAlert("Enter comment text");
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/CreateCommentAsync",
                data: { PostID: postID, CommentID: commenID, Text: text }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        blog.comments.getComments();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '.edit-comment-delete', function (e) {
            e.preventDefault();

            var commentID = $(e.currentTarget).data('id');

            bootbox.confirm("All child comments for this comment will be deleted as well", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeleteComment",
                        data: { commentID: commentID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg, true);
                                blog.comments.getComments();
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });
    },

    //Getting comments by ajax request and render response result
    getComments: function () {
        $.ajax({
            method: "POST",
            url: "/Admin/Home/GetComments",
            data: { postID: blog.comments.options.postID }
        })
            .done(function (data) {
                if (data.result) {
                    blog.comments.render(data);
                }
                else {
                    ShowBlogAlert("There are no data or data server unaccesible");
                }
            });
    },

    render: function (data) {
        $('.comments-wrapper').empty();
        var postTemplate = $.templates('#post-comment-template');
        $.each(data.posts, function (key, item) {
            var htmlOutput = postTemplate.render(item);
            $('.comments-wrapper').append(htmlOutput);
        });

        var commentTemplate = $.templates('#comment-template');
        $.each(data.items, function (key, item) {
            if (item.commentID === 0) {
                var htmlOutput = commentTemplate.render(item);
                $('#post-comments-wrapper-' + item.postID).prepend(htmlOutput);
            }
            else if (item.commentID > 0) {
                blog.comments.renderChild(item);
            }
        });
    },

    renderChild: function (item) {
        var commentTemplate = $.templates("#comment-template");
        var htmlOutput = commentTemplate.render(item);
        $('#child-comments-wrapper-' + item.commentID).prepend(htmlOutput);
    }
};