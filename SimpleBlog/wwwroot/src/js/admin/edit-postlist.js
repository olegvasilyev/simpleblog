﻿var blog = blog || {};

//blog.postlist is a simple component to manage post list which is rendered in datatable
blog.postlist = {
    init: function () {
        $document = $(document);

        $document.on('click', 'button.delete-post', function (e) {
            e.preventDefault();

            var postID = $(e.currentTarget).data('postid');

            if (!postID || postID === "") {
                return;
            }

            bootbox.confirm("All comments for this post will be deleted with post", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeletePost",
                        data: { postID: postID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg);
                                postTable.ajax.reload();
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });

        $document.on('change', '.published-status', function (e) {
            e.preventDefault();

            var element = $(e.currentTarget);
            var postID = element.data('postid');
            var isPublished = element.is(":checked");

            $.ajax({
                method: "POST",
                url: "/Admin/Home/SetPublishedStatus",
                data: { postID: postID, isPublished: isPublished }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        postTable.ajax.reload();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });
    }
};