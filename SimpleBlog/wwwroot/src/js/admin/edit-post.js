﻿var blog = blog || {};

//blog.editPost is a component to edit or create post
blog.editPost = {
    options: {
        isNewPost: true
    },
    init: function () {
        blog.editPost.options.isNewPost = currentPostID === 0 ? true : false;

        $("#edit-post-form").submit(function (e) {
            e.preventDefault();

            var title = $('#edit-post-title').val();
            var description = $('#edit-post-descryption').val();
            var text = $('#edit-post-text').val();
            var isPublished = $('#edit-post-is-published').is(":checked");

            $("span[class*='validation-info']").addClass('invisible');

            if (!title || title === "") {
                $('#edit-post-title').focus();
                if ($('.title-validation-info').hasClass('invisible')) {
                    $('.title-validation-info').removeClass('invisible');
                }
                return;
            }

            if (!description || description === "") {
                $('#edit-post-descryption').focus();
                if ($('.descryption-validation-info').hasClass('invisible')) {
                    $('.descryption-validation-info').removeClass('invisible');
                }
                return;
            }

            if (!text || text === "") {
                $('#edit-post-text').focus();
                if ($('.text-validation-info').hasClass('invisible')) {
                    $('.text-validation-info').removeClass('invisible');
                }
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/EditPostAsync",
                data: { Id: currentPostID, Title: title, Description: description, Text: text, IsPublished: isPublished }
            })
                .done(function (data) {
                    if (data.result) {
                        window.location = "/admin/Home/EditPost/" + data.postID;
                    }
                });
        });

        $document = $(document);

        $document.on('click', '#delete-post-button', function (e) {
            e.preventDefault();

            var postID = $('#delete-post-button').data('postid');
            if (!postID || postID === 0) {
                return;
            }

            bootbox.confirm("All comments for this post will be deleted with post", function (result) {
                if (result) {
                    $.ajax({
                        method: "POST",
                        url: "/Admin/Home/DeletePost",
                        data: { postID: postID }
                    })
                        .done(function (data) {
                            if (data.result) {
                                ShowBlogAlert(data.msg, true);
                            }
                            else {
                                ShowBlogAlert(data.msg);
                            }
                        });
                }
            });
        });

        $document.on('change', 'input[type="checkbox"].is-checked', function (e) {
            e.preventDefault();

            if (blog.editPost.options.isNewPost) {
                ShowBlogAlert('You can not set or unset tag to unsaved post. Please save post before');
                return;
            }

            var element = $(e.currentTarget);
            var tagID = element.data('tagid');
            var isChecked = element.is(":checked");

            $.ajax({
                method: "POST",
                url: "/Admin/Home/SetTagToPost",
                data: { postID: currentPostID, tagID: tagID, isChecked: isChecked }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '#new-tag-submit', function (e) {
            e.preventDefault();

            var name = $('#new-tag-name').val();

            if (!name || name === "") {
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/CreateTag",
                data: { name: name }
            })
                .done(function (data) {
                    if (data.result) {
                        $('#new-tag-name').val('');
                        ShowBlogAlert(data.msg);
                        var template = $.templates("#tag-template");
                        var htmlOutput = template.render(data);
                        $('.tags-wrapper').append(htmlOutput);
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });

        $document.on('click', '.tag-group i.close-btn', function (e) {
            e.preventDefault();

            var element = $(e.currentTarget);
            var tagID = element.data('tagid');

            if (!tagID || tagID === "") {
                return;
            }

            $.ajax({
                method: "POST",
                url: "/Admin/Home/DeleteTag",
                data: { tagID: tagID }
            })
                .done(function (data) {
                    if (data.result) {
                        ShowBlogAlert(data.msg);
                        element.closest('.tag-group').remove();
                    }
                    else {
                        ShowBlogAlert(data.msg);
                    }
                });
        });
    }
};

