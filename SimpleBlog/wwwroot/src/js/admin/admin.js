﻿//Show popup alert
function ShowBlogAlert(msg, isReload, time) {

    if (!isReload) {
        isReload = false;
    }

    if (!time) {
        time = 3000;
    }

    var dialog = bootbox.dialog({
        message: msg,
    });

    setTimeout(function () {
        dialog.modal('hide');
        if (isReload) {
            location.reload();
        }
    }, time);
};