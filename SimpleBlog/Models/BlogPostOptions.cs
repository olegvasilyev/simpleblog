﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Models
{
    public class BlogPostOptions
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TagID { get; set; }
        public string Search { get; set; }
    }
}
