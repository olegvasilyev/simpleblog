﻿using SimpleBlog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Models
{
    //PostListViewModel is a view model that is returned to user in Json format
    public class PostListViewModel
    {
        public List<BlogPostViewModel> Posts { get; set; }
        public int Page { get; set; }
        public int Pages { get; set; }
        public int TagID { get; set; }
    }
}

//BlogPostViewModel class describe post item view model
public class BlogPostViewModel
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Text { get; set; }
    public bool IsPublished { get; set; }
    public string Date { get; set; }
    public string PostedBy { get; set; }
    public int CommentsCount { get; set; }
    public List<PostTagViewModel> Tags { get; set; }
    public List<PostCommentViewModel> Comments { get; set; }
}

//PostTagViewModel class describe post tag item view model, that is part of BlogPostViewModel as a list of items
public class PostTagViewModel
{
    public int Id { get; set; }
    public string Name { get; set; }
}

//PostCommentViewModel class describe comment item view model, that is part of BlogPostViewModel as a list of items
public class PostCommentViewModel
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public string Text { get; set; }
    public string Created { get; set; }
    public int PostID { get; set; }
    public int CommentID { get; set; }
}


