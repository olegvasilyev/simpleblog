﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Models
{
    //This class is designed to get and map parameters from Ajax HttpRequest to get strongly typed options for datatable
    public class AjaxModels
    {
        public static DataTableModel GetDataTableParameters(HttpRequest request)
        {
            var form = request.Form;
            var res = new DataTableModel();
            try
            {
                res.Draw = Convert.ToInt32(form["draw"].ToString());
                res.Skip = Convert.ToInt32(form["start"].ToString());
                res.PageSize = Convert.ToInt32(form["length"].ToString());
                res.ColumnNumber = Convert.ToInt32(form["order[0][column]"].ToString());
                res.ColumnName = form[$"columns[{res.ColumnNumber}][data]"].ToString();
                res.Search = form["search[value]"].ToString();
                res.SortDirection = form["order[0][dir]"].ToString();
            }
            catch (Exception)
            {
            }
            return res;
        }
    }

    public class DataTableModel
    {
        public int Draw { get; set; }
        public int Skip { get; set; }
        public int PageSize { get; set; }
        public string Search { get; set; }
        public int ColumnNumber { get; set; }
        public string ColumnName { get; set; }
        public string SortDirection { get; set; }
    }
}

