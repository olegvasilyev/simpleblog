﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Models
{
    public class NewCommentViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public int PostID { get; set; }
        public int CommentID { get; set; }
    }
}
