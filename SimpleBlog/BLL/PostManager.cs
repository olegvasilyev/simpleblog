﻿using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.DAL;
using SimpleBlog.DAL.Entities;
using SimpleBlog.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.BLL
{
    //PostManager is a class that contains all bussiness logic to work with post items, tags, comments and errors.
    //All critical parts of code enclosed into try/catch sections to prevent application's crash
    public class PostManager : IPostManager
    {
        #region System
        private IRepository _db;
        private bool _disposed;

        public PostManager(IRepository db)
        {
            this._db = db;
            _disposed = false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) { }
                _disposed = true;
            }
        }

        //ErrorLog logs all exceptions information into database
        private void ErrorLog(string text, string additional = "")
        {
            Error error = new Error
            {
                Id = 0,
                ErrorText = text,
                Additional = additional,
                Occurred = DateTime.Now
            };
            _db.SaveError(error);
        }

        //Async pair to ErrorLog method above with the same purposes
        private async Task ErrorLogAsync(string text, string additional = "")
        {
            Error error = new Error
            {
                Id = 0,
                ErrorText = text,
                Additional = additional,
                Occurred = DateTime.Now
            };
            await _db.SaveErrorAsync(error);
        }
        #endregion

        //GetAllTags method returns all tag's collection mapped to PostTagsModel list
        public List<PostTagsModel> GetAllTags()
        {
            var res = new List<PostTagsModel>();
            try
            {
                res = _db.GetTags()
                   .Select(x => new PostTagsModel
                   {
                       Id = x.Id,
                       Name = x.Name,
                       IsSelected = false
                   }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, "GetAllTags");
            }
            return res;
        }

        //GetAllTags method returns all tag's list
        public List<Tag> GetTags()
        {
            var res = new List<Tag>();
            try
            {
                res = _db.GetTags().ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, "GetTags");
            }
            return res;
        }

        //GetPost retrive post item by it's id and map it to the PostViewModel class.
        //This method is used to get post to edit in user cabinet.
        public PostViewModel GetPost(int postID)
        {
            var res = new PostViewModel();

            try
            {
                if (postID > 0)
                {
                    var posTags = _db.GetPostTags().Where(x => x.PostID == postID).Select(s => s.TagID);

                    var tags = _db.GetTags()
                            .Select(x => new PostTagsModel
                            {
                                Id = x.Id,
                                Name = x.Name,
                                IsSelected = posTags.Contains(x.Id)
                            }).ToList();

                    res = _db.GetPosts()
                        .Where(x => x.Id == postID)
                        .Select(s => new PostViewModel
                        {
                            Id = s.Id,
                            Title = s.Title,
                            Text = s.Text,
                            Description = s.Description,
                            IsPublished = s.Published,
                            Posted = s.Posted.ToString("MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture),
                            Modified = s.Modified.HasValue ? s.Modified.Value.ToString("MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture) : "",
                            Tags = tags
                        }).FirstOrDefault();
                }
                else
                {
                    res.Posted = "";
                    res.Modified = "";
                    res.Tags = GetAllTags();
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"GetPost: id = {postID}");
            }
            return res;
        }

        //GetPosts method returns post collection that is processed by filtering, sorting, paging and mapping rules for datatable.
        public PostDataModel GetPosts(DataTableModel parameters)
        {
            var res = new PostDataModel();

            try
            {
                var items = _db.GetPosts();

                res.RecordsTotal = items.Count();
                res.Draw = parameters.Draw;
                res.Error = "";

                //If request has non-empty search text option - post items query include filter options
                items = items.Where(x => String.IsNullOrEmpty(parameters.Search) || x.Title.Contains(parameters.Search));

                //Sorting part
                switch (parameters.ColumnName)
                {
                    case "title":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Title);
                        else items = items.OrderByDescending(x => x.Title);
                        break;
                    case "posted":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Posted);
                        else items = items.OrderByDescending(x => x.Posted);
                        break;
                    case "modified":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Modified);
                        else items = items.OrderByDescending(x => x.Modified);
                        break;
                    case "comments":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Comments.Count);
                        else items = items.OrderByDescending(x => x.Comments.Count);
                        break;
                    case "published":
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Published);
                        else items = items.OrderByDescending(x => x.Published);
                        break;
                    default:
                        if (parameters.SortDirection == "asc") items = items.OrderBy(x => x.Title);
                        else items = items.OrderByDescending(x => x.Title);
                        break;
                }

                //Paging and mapping part
                res.RecordsFiltered = items.ToList().Count();
                res.Data = items.Skip(parameters.Skip)
                    .Take(parameters.PageSize)
                    .ToList()
                    .Select(x => new PostDataItemModel
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Posted = x.Posted.ToString("MM/dd/yyyy HH:mm"),
                        Modified = x.Modified.HasValue ? x.Modified.Value.ToString("MM/dd/yyyy HH:mm") : "no changes",
                        Published = x.Published,
                        Comments = x.Comments.Count
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, "GetPosts");
                res.Error = ex.Message;
            }
            return res;
        }

        //EditPost method edit existing post or create new one. Workflow depends on id parameter.
        public Post EditPost(int id, string title, string descryption, string text, bool isPiblished, ApplicationUser user, out string msg)
        {
            msg = "";
            Post res = new Post();
            try
            {
                if (id > 0)
                {
                    res = _db.GetPosts().FirstOrDefault(x => x.Id == id);
                    res.Title = title;
                    res.Description = descryption;
                    res.Text = text;
                    res.Published = isPiblished;
                    res.Modified = DateTime.Now;
                    _db.SavePost(res);
                    msg = "Post was saved succsessfully";
                }
                else if (id == 0)
                {
                    res.Id = id;
                    res.Title = title;
                    res.Description = descryption;
                    res.Text = text;
                    res.Published = isPiblished;
                    res.Posted = DateTime.Now;
                    res.PostedBy = user.UserName;
                    res.AuthorName = user.FullName;
                    _db.SavePost(res);
                    msg = "Post was saved succsessfully";
                }
                else
                {
                    msg = "Post was not saved";
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"EditPost: id = {id}, username = {user.UserName}");
                msg = ex.Message;
            }
            return res;
        }

        public bool DeletePost(int postID, out string msg)
        {
            msg = "";
            var res = false;
            try
            {
                res = _db.DeletePost(postID);
                msg = "Post was deleted successfully";
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"DeletePost by postID = {postID}");
                msg = ex.Message;
            }
            return res;
        }

        public async Task<bool> DeletePostAsync(int postID)
        {
            var res = false;
            try
            {
                res = await _db.DeletePostAsync(postID);

            }
            catch (Exception ex)
            {
                await ErrorLogAsync(ex.Message, $"DeletePost by postID = {postID}");
            }
            return res;
        }

        public bool DeleteTag(int tagID, out string msg)
        {
            msg = "";
            var res = false;
            try
            {
                res = _db.DeleteTag(tagID);
                msg = "Tag was deleted successfully";
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"DeleteTag by tagID = {tagID}");
                msg = ex.Message;
            }
            return res;
        }

        public async Task<bool> DeleteTagAsync(int tagID)
        {
            var res = false;
            try
            {
                res = await _db.DeleteTagAsync(tagID);
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"DeleteTag by tagID = {tagID}");
            }
            return res;
        }

        //SetTagToPost method sets selected tag to selected post
        public bool SetTagToPost(int postID, int tagID, bool isChecked, out string msg)
        {
            msg = "";
            var item = new PostTag();
            var res = false;
            try
            {
                if (postID > 0 && tagID > 0)
                {
                    var post = _db.GetPosts().FirstOrDefault(x => x.Id == postID);
                    var tag = _db.GetTags().FirstOrDefault(x => x.Id == tagID);
                    if (post != null && tag != null)
                    {
                        if (isChecked)
                        {
                            item.Id = 0;
                            item.PostID = postID;
                            item.TagID = tagID;
                            _db.SavePostTag(item);
                            msg = "Tag was setted to current post successfully";
                            res = true;
                        }
                        else if (!isChecked)
                        {
                            item = _db.GetPostTags().FirstOrDefault(x => x.PostID == postID && x.TagID == tagID);
                            if (item != null && item.Id > 0)
                            {
                                res = _db.DeletePostTag(item.Id);
                                msg = "Tag was unsetted from current post successfully";
                            }
                        }
                    }
                }
                else if (postID == 0)
                {
                    msg = "Please, save post before set or unset tag to it";
                }
                else
                {
                    msg = "Tag was not setted to post";
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"Create PostTag by postID = {postID}, tagID = {tagID}");
                msg = ex.Message;
            }
            return res;
        }

        //SetPublishedStatus method change post's published status, which means the post will be shown to blog guests or not. 
        public bool SetPublishedStatus(int postID, bool isPublished, out string msg)
        {
            msg = "";
            var item = new PostTag();
            var res = false;
            try
            {
                var post = _db.GetPosts().FirstOrDefault(x => x.Id == postID);
                post.Published = isPublished;
                _db.SavePost(post);
                res = true;
                msg = "Status changed successfully";
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"Set published status by postID = {postID}");
                msg = ex.Message;
            }
            return res;
        }

        public Tag CreateTag(string name, out string msg)
        {
            msg = "";
            var res = new Tag();
            try
            {
                var item = _db.GetTags().FirstOrDefault(x => x.Name.Equals(name));
                if (item == null && !String.IsNullOrEmpty(name))
                {
                    res.Id = 0;
                    res.Name = name;
                    _db.SaveTag(res);
                    msg = "Tag was saved successfully";
                }
                else
                {
                    msg = "Tag was not created";
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"Create tag by name = {name}");
                msg = ex.Message;
            }
            return res;
        }

        //GetComments returns all comments or commets to post with id equals to postID. Than result mappet to CommentsModel.
        public CommentsModel GetComments(int postID)
        {
            var res = new CommentsModel();
            List<Comment> comments;

            try
            {
                if (postID > 0)
                {
                    comments = _db.GetComments().Where(x => x.PostID == postID).OrderBy(x => x.Created).ToList();
                }
                else
                {
                    comments = _db.GetComments().OrderBy(x => x.Created).ToList();
                }

                res.Posts = comments.Select(x => x.Post)
                    .Distinct()
                    .OrderByDescending(x => x.Posted)
                    .Select(x => new PostItemModel
                    {
                        PostID = x.Id,
                        Author = x.AuthorName,
                        Title = x.Title,
                        Posted = x.Posted.ToString("MM/dd/yyyy HH:mm"),
                        Modified = x.Modified.HasValue ? x.Modified.Value.ToString("MM/dd/yyyy HH:mm") : ""

                    }).ToList();

                res.Items = comments.Select(x => new CommenItemModel
                {
                    Id = x.Id,
                    UserName = x.UserName,
                    Email = x.Email,
                    Text = x.Text,
                    Created = x.Created.ToString("MM/dd/yyyy HH:mm"),
                    PostID = x.PostID,
                    CommentID = x.CommentID.HasValue ? x.CommentID.Value : 0,
                    IsVisible = x.IsVisible,
                }).ToList();

                res.Total = res.Items.Count;
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"GetComments with postID = {postID}");
            }
            return res;
        }

        public bool DeleteComment(int commentID, out string msg)
        {
            msg = "";
            var res = false;
            try
            {
                res = _db.DeleteComment(commentID);
                msg = "Comment was deleted successfully";
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"DeleteComment by commentID = {commentID}");
                msg = ex.Message;
            }
            return res;
        }

        //ChangeCommentVisibleStatus change comment's visible status. If it sets to invisible result wil hide all child comments as well.
        public bool ChangeCommentVisibleStatus(int commentID, bool isVisible, out string msg)
        {
            msg = "";
            var res = false;
            try
            {
                var item = _db.GetComments().FirstOrDefault(x => x.Id == commentID);
                if (item != null)
                {
                    item.IsVisible = isVisible;
                    _db.SaveComment(item, true);
                    msg = "Comment visible status was changed successfully";
                    res = true;
                }
                else
                {
                    msg = "Comment visible status was not changed";
                    res = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"ChangeCommentVisibleStatus by commentID = {commentID}");
                msg = ex.Message;
            }
            return res;
        }

        public Comment CreateComment(int postID, int commentID, string text, ApplicationUser user, out string msg)
        {
            var res = new Comment();
            msg = "";
            try
            {
                var item = new Comment
                {
                    Id = 0,
                    UserName = user.FullName,
                    Email = user.UserName,
                    Text = text,
                    PostID = postID,
                    Created = DateTime.Now,
                    IsVisible = true,
                    CommentID = commentID
                };
                _db.SaveComment(item);

                res = _db.GetComments().FirstOrDefault(x => x.Id == item.Id);
                msg = "Comment was saved successfully";
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"CreateComment for postID = {postID}, username = {user.UserName}");
                msg = ex.Message;
            }

            return res;
        }

        public async Task<Comment> CreateCommentAsync(NewCommentViewModel model)
        {
            var res = new Comment();
            try
            {
                var item = new Comment
                {
                    Id = 0,
                    UserName = model.UserName,
                    Email = model.Email,
                    Text = model.Text,
                    PostID = model.PostID,
                    Created = DateTime.Now,
                    IsVisible = true,
                    CommentID = model.CommentID == 0 ? null : (int?)model.CommentID
                };
                await _db.SaveCommentAsync(item);

                res = _db.GetComments().FirstOrDefault(x => x.Id == item.Id);
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, $"CreateComment for postID = {model.PostID}, username = {model.UserName}");
            }

            return res;
        }

        public async Task<bool> DeleteCommentAsync(int commentID)
        {
            var res = false;
            try
            {
                res = await _db.DeleteCommentAsync(commentID);
            }
            catch (Exception ex)
            {
                await ErrorLogAsync(ex.Message, $"DeleteComment by commentID = {commentID}");
            }
            return res;
        }

        //GetBlogPosts complex method selects, process and map to output view model posts for front end post list.
        public PostListViewModel GetBlogPosts(BlogPostOptions options)
        {
            var res = new PostListViewModel();

            try
            {
                res.Page = options.Page;
                res.TagID = options.TagID;

                //Filtering section. Processing depents on search text and post tag that is implemented by user.
                var postIDs = _db.GetPostTags().Where(x => x.TagID.Equals(options.TagID)).Select(x => x.PostID);

                IQueryable<Post> items = _db.GetPosts().Where(x => x.Published && (options.TagID == 0 || postIDs.Contains(x.Id)) &&
                (String.IsNullOrEmpty(options.Search) || (x.Title.Contains(options.Search) || x.Description.Contains(options.Search) || x.Text.Contains(options.Search))))
                    .OrderByDescending(x => x.Modified ?? x.Posted);

                //Paging section
                var totalFilteredItems = items.Count();

                res.Pages = (totalFilteredItems % options.PageSize) > 0 ? (totalFilteredItems / options.PageSize) + 1 : totalFilteredItems / options.PageSize;

                //Mapping section. Selected posts map to BlogPostViewModel with attaching it's own comments and tags collections.
                res.Posts = items.Skip(options.PageSize * (options.Page - 1)).Take(options.PageSize)
                    .Select(x => new BlogPostViewModel
                    {
                        Id = x.Id,
                        Title = x.Title,
                        Description = x.Description,
                        Text = x.Text,
                        Date = x.Modified.HasValue ? $"{x.Posted.ToShortDateString()}, modified {x.Modified.Value.ToShortDateString()}" : x.Posted.ToShortDateString(),
                        PostedBy = x.AuthorName,
                        IsPublished = x.Published,
                        CommentsCount = x.Comments.Where(c => c.IsVisible).Count(),
                        Comments = _db.GetComments().Where(c => c.IsVisible && c.PostID == x.Id)
                        .OrderBy(c => c.Created)
                        .Select(c => new PostCommentViewModel
                        {
                            Id = c.Id,
                            UserName = c.UserName,
                            Text = c.Text,
                            Created = c.Created.ToShortDateString(),
                            PostID = c.PostID,
                            CommentID = c.CommentID ?? 0
                        }).ToList(),
                        Tags = _db.GetPostTags().Where(y => y.PostID == x.Id)
                        .Select(i => new PostTagViewModel
                        {
                            Id = i.Id,
                            Name = i.Tag.Name
                        }).ToList()
                    }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message, "GetBlogPosts");
            }
            return res;
        }
    }
}
