﻿using Microsoft.Extensions.Options;
using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.DAL.Entities;
using SimpleBlog.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlog.BLL
{
    public interface IPostManager : IDisposable
    {
        PostDataModel GetPosts(DataTableModel parameters);
        Post EditPost(int id, string title, string descryption, string text, bool isPiblished, ApplicationUser user, out string msg);
        PostViewModel GetPost(int postID);
        List<PostTagsModel> GetAllTags();
        List<Tag> GetTags();
        bool DeletePost(int postID, out string msg);
        Task<bool> DeletePostAsync(int postID);
        bool DeleteTag(int tagID, out string msg);
        Task<bool> DeleteTagAsync(int tagID);
        bool SetTagToPost(int postID, int tagID, bool isChecked, out string msg);
        Tag CreateTag(string name, out string msg);
        bool SetPublishedStatus(int postID, bool isPublished, out string msg);
        CommentsModel GetComments(int postID = 0);
        bool DeleteComment(int commentID, out string msg);
        bool ChangeCommentVisibleStatus(int commentID, bool isVisible, out string msg);
        Comment CreateComment(int postID, int commentID, string text, ApplicationUser user, out string msg);
        Task<Comment> CreateCommentAsync(NewCommentViewModel model);
        Task<bool> DeleteCommentAsync(int commentID);
        PostListViewModel GetBlogPosts(BlogPostOptions options);
    }
}
