﻿using Microsoft.EntityFrameworkCore;
using SimpleBlog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.DAL
{
    //This class contains data accsess layer to work wit entities using Entity framework.
    //Each entity has three methods to delete, save and get ones.
    //To work with some entities here are implemented async methods as a pair to synchronous ones.
    public class Repository : IRepository
    {
        #region System
        private ApplicationDbContext db;

        private bool _disposed;
        public Repository(ApplicationDbContext context)
        {
            this.db = context;
            _disposed = false;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (db != null) db.Dispose();
                }
                db = null;
                _disposed = true;
            }
        }
        #endregion

        #region Posts
        public IQueryable<Post> GetPosts()
        {
            return db.Posts.Include(x => x.PostTags).Include(c => c.Comments);
        }

        public bool DeletePost(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Posts.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) Save();
                res = true;
            }
            return res;
        }

        public int SavePost(Post item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Posts.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) Save();
            return item.Id;
        }

        public async Task<bool> DeletePostAsync(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Posts.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) await SaveAsync();
                res = true;
            }
            return res;
        }

        public async Task<int> SavePostAsync(Post item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Posts.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) await SaveAsync();
            return item.Id;
        }

        #endregion

        #region Tags
        public IQueryable<Tag> GetTags()
        {
            return db.Tags.Include(x => x.PostTags);
        }

        public bool DeleteTag(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Tags.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) Save();
                res = true;
            }
            return res;
        }

        public int SaveTag(Tag item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Tags.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) Save();
            return item.Id;
        }

        public async Task<bool> DeleteTagAsync(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Tags.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) await SaveAsync();
                res = true;
            }
            return res;
        }

        public async Task<int> SaveTagAsync(Tag item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Tags.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) await SaveAsync();
            return item.Id;
        }
        #endregion

        #region PostTags
        public IQueryable<PostTag> GetPostTags()
        {
            return db.PostTags
                .Include(p => p.Post)
                .Include(t => t.Tag);
        }

        public bool DeletePostTag(int id, bool withSave = true)
        {
            var res = false;
            var item = db.PostTags.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) Save();
                res = true;
            }
            return res;
        }

        public int SavePostTag(PostTag item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.PostTags.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) Save();
            return item.Id;
        }

        public async Task<bool> DeletePostTagAsync(int id, bool withSave = true)
        {
            var res = false;
            var item = db.PostTags.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                db.Entry(item).State = EntityState.Deleted;
                if (withSave) await SaveAsync();
                res = true;
            }
            return res;
        }

        public async Task<int> SavePostTagAsync(PostTag item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.PostTags.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) await SaveAsync();
            return item.Id;
        }
        #endregion

        #region Comments
        public IQueryable<Comment> GetComments()
        {
            return db.Comments.Include(x => x.Post).Include(x => x.Comments);
        }

        //DeleteComment method works with DeleteChildComments to delete recursively all child items as well.
        public bool DeleteComment(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Comments.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                var childs = db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToList();

                if (childs != null)
                {
                    DeleteChildComments(childs);
                }

                db.Entry(item).State = EntityState.Deleted;
                if (withSave) Save();
                res = true;
            }
            return res;
        }

        private void DeleteChildComments(List<Comment> items)
        {

            foreach (var item in items)
            {
                var childs = db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToList();
                if (childs != null)
                {
                    DeleteChildComments(childs);
                }
                db.Entry(item).State = EntityState.Deleted;
            }
        }

        public int SaveComment(Comment item, bool isVisibleChanged = false, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Comments.Add(item);
            }
            else
            {
                if (isVisibleChanged)
                {
                    var childs = db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToList();
                    if (childs != null)
                    {
                        SaveChildComments(childs, item.IsVisible);
                    }
                }

                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) Save();
            return item.Id;
        }

        private void SaveChildComments(List<Comment> items, bool isVisible)
        {

            foreach (var item in items)
            {
                item.IsVisible = isVisible;
                var childs = db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToList();
                if (childs != null)
                {
                    SaveChildComments(childs, isVisible);
                }
                db.Entry(item).State = EntityState.Modified;
            }
        }
        public async Task<int> SaveCommentAsync(Comment item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Comments.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) await SaveAsync();
            return item.Id;
        }

        public async Task<bool> DeleteCommentAsync(int id, bool withSave = true)
        {
            var res = false;
            var item = db.Comments.SingleOrDefault(x => x.Id == id);
            if (item != null)
            {
                var childs = await db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToListAsync();

                if (childs != null)
                {
                    await DeleteChildCommentsAsync(childs);
                }

                db.Entry(item).State = EntityState.Deleted;
                if (withSave) await SaveAsync();
                res = true;
            }
            return res;
        }

        private async Task DeleteChildCommentsAsync(List<Comment> items)
        {

            foreach (var item in items)
            {
                var childs = await db.Comments.Where(x => x.CommentID.HasValue && x.CommentID.Value == item.Id).ToListAsync();
                if (childs != null)
                {
                    await DeleteChildCommentsAsync(childs);
                }
                db.Entry(item).State = EntityState.Deleted;
            }
        }

        #endregion

        #region Errors
        public IQueryable<Error> GetErrors()
        {
            return db.Errors;
        }

        public int SaveError(Error item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Errors.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) Save();
            return item.Id;
        }

        public async Task<int> SaveErrorAsync(Error item, bool withSave = true)
        {
            if (item.Id == 0)
            {
                db.Errors.Add(item);
            }
            else
            {
                db.Entry(item).State = EntityState.Modified;
            }
            if (withSave) await SaveAsync();
            return item.Id;
        }
        #endregion
    }
}
