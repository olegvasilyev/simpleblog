﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.DAL.Entities
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        [StringLength(1024)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public bool Published { get; set; }

        [Required]
        public DateTime Posted { get; set; }

        [Required]
        [StringLength(256)]
        public string PostedBy { get; set; }

        [Required]
        [StringLength(256)]
        public string AuthorName { get; set; }

        public DateTime? Modified { get; set; }

        public ICollection<PostTag> PostTags { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
