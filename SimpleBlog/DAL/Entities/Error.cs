﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.DAL.Entities
{
    public class Error
    {
        public int Id { get; set; }

        [Required]
        public string ErrorText { get; set; }

        [Required]
        public DateTime Occurred { get; set; }

        [StringLength(1024)]
        public string Additional { get; set; }
    }
}
