﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.DAL.Entities
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [Required]
        [StringLength(128)]
        public string Email { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public int PostID { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        public int? CommentID { get; set; }

        public Comment TargetComment { get; set; }

        public Post Post { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
