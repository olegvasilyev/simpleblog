﻿using SimpleBlog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.DAL
{
    public interface IRepository : IDisposable
    {
        #region System
        void Save();
        Task SaveAsync();
        #endregion

        #region Posts
        IQueryable<Post> GetPosts();
        bool DeletePost(int id, bool withSave = true);
        int SavePost(Post item, bool withSave = true);
        Task<bool> DeletePostAsync(int id, bool withSave = true);
        Task<int> SavePostAsync(Post item, bool withSave = true);

        #endregion

        #region Tags
        IQueryable<Tag> GetTags();
        bool DeleteTag(int id, bool withSave = true);
        int SaveTag(Tag item, bool withSave = true);
        Task<bool> DeleteTagAsync(int id, bool withSave = true);
        Task<int> SaveTagAsync(Tag item, bool withSave = true);
        #endregion

        #region PostTags
        IQueryable<PostTag> GetPostTags();
        bool DeletePostTag(int id, bool withSave = true);
        int SavePostTag(PostTag item, bool withSave = true);
        Task<bool> DeletePostTagAsync(int id, bool withSave = true);
        Task<int> SavePostTagAsync(PostTag item, bool withSave = true);
        #endregion

        #region Comments
        IQueryable<Comment> GetComments();
        bool DeleteComment(int id, bool withSave = true);
        int SaveComment(Comment item, bool isVisibleChanged = false, bool withSave = true);
        Task<int> SaveCommentAsync(Comment item, bool withSave = true);
        Task<bool> DeleteCommentAsync(int id, bool withSave = true);
        #endregion

        #region Errors
        IQueryable<Error> GetErrors();
        int SaveError(Error item, bool withSave = true);
        Task<int> SaveErrorAsync(Error item, bool withSave = true);
        #endregion
    }
}
