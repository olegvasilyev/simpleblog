﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog
{
    public class Settings
    {
        public string AppName { get; set; }
        public string Version { get; set; }
        public String Author { get; set; }
    }

    public class BlogSettings
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int MaxVisible { get; set; }
        public string ReCAPTCHAKey { get; set; }
    }
}
