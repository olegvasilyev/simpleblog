﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.Controllers;
using Microsoft.AspNetCore.Identity;
using SimpleBlog.BLL;
using SimpleBlog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(UserManager<ApplicationUser> userManager, IPostManager mng)
            : base(mng)
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult GetPosts()
        {
            var parameters = AjaxModels.GetDataTableParameters(Request);
            var res = mng.GetPosts(parameters);
            return Json(new
            {
                draw = res.Draw,
                recordsTotal = res.RecordsTotal,
                recordsFiltered = res.RecordsFiltered,
                data = res.Data,
                error = res.Error
            });
        }

        [HttpGet]
        public IActionResult EditPost(int postID = 0)
        {
            var vm = mng.GetPost(postID);
            if (vm == null)
            {
                return RedirectToAction("Index", "Home", new { area = "admin" });
            }
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> EditPostAsync(EditPostViewModel model)
        {
            var res = false;
            var postID = 0;
            var msg = "";
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                if (user != null)
                {
                    var item = mng.EditPost(model.Id, model.Title, model.Description, model.Text, model.IsPublished, user, out msg);
                    if (item != null && item.Id > 0)
                    {
                        res = true;
                        postID = item.Id;
                    }
                }
            }
            else
            {
                msg = "Data was incorrect. Post is not saved";
            }
            return Json(new
            {
                result = res,
                msg,
                postID
            });
        }

        [HttpPost]
        public IActionResult CreateTag(string name)
        {
            var msg = "";
            var res = mng.CreateTag(name, out msg);
            return Json(new
            {
                result = res != null || res.Id > 0,
                name = res.Name,
                tagID = res.Id,
                msg
            });
        }

        [HttpPost]
        public IActionResult SetTagToPost(int postID, int tagID, bool isChecked)
        {
            var msg = "";
            var res = mng.SetTagToPost(postID, tagID, isChecked, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        [HttpPost]
        public IActionResult DeletePost(int postID)
        {
            var msg = "";
            var res = mng.DeletePost(postID, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        [HttpPost]
        public IActionResult DeleteTag(int tagID)
        {
            var msg = "";
            var res = mng.DeleteTag(tagID, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        [HttpPost]
        public IActionResult SetPublishedStatus(int postID, bool isPublished)
        {
            var msg = "";
            var res = mng.SetPublishedStatus(postID, isPublished, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        public IActionResult Comments(int postID)
        {
            return View(postID);
        }

        [HttpPost]
        public IActionResult GetComments(int postID)
        {
            var res = mng.GetComments(postID);
            return Json(new
            {
                result = res != null,
                posts = res.Posts,
                items = res.Items,
                total = res.Total
            });
        }

        [HttpPost]
        public IActionResult DeleteComment(int commentID)
        {
            var msg = "";
            var res = mng.DeleteComment(commentID, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        [HttpPost]
        public IActionResult ChangeVisibleStatus(int commentID, bool isVisible)
        {
            var msg = "";
            var res = mng.ChangeCommentVisibleStatus(commentID, isVisible, out msg);
            return Json(new
            {
                result = res,
                msg
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateCommentAsync(CommentViewModel model)
        {
            var res = false;
            var msg = "";
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                if (user != null)
                {
                    var item = mng.CreateComment(model.PostID, model.CommentID, model.Text, user, out msg);
                    if (item != null && item.Id > 0)
                    {
                        res = true;
                    }
                }
            }
            else
            {
                msg = "Data was incorrect. Comment is not saved";
            }
            return Json(new
            {
                result = res,
                msg,
            });
        }
    }
}