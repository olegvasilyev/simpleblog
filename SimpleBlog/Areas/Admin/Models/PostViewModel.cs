﻿using SimpleBlog.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(1024)]
        public string Title { get; set; }

        [Required]
        [MinLength(5)]
        public string Description { get; set; }

        [Required]
        [MinLength(5)]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Published")]
        public bool IsPublished { get; set; }

        [Display(Name = "Posted")]
        public string Posted { get; set; }

        [Display(Name = "Modified")]
        public string Modified { get; set; }

        public List<PostTagsModel> Tags { get; set; }
    }

    public class EditPostViewModel
    {
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(1024)]
        public string Title { get; set; }

        [Required]
        [MinLength(5)]
        public string Description { get; set; }

        [Required]
        [MinLength(5)]
        public string Text { get; set; }

        [Required]
        public bool IsPublished { get; set; }
    }

    public class PostTagsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }


}
