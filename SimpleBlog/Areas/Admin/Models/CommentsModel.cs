﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Models
{
    public class CommentsModel
    {
        public List<CommenItemModel> Items { get; set; }
        public List<PostItemModel>Posts{ get; set; }
        public int Total { get; set; }
    }

    public class CommenItemModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
        public string Created { get; set; }
        public int PostID { get; set; }
        public int CommentID { get; set; }
        public bool IsVisible { get; set; }
    }

    public class PostItemModel
    {
        public int PostID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Posted { get; set; }
        public string Modified { get; set; }
    }
}
