﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Models
{
    public class CommentViewModel
    {
        public int PostID { get; set; }
        public int CommentID { get; set; }
        public string Text { get; set; }
    }
}
