﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Models
{
    public class PostDataModel
    {
        public List<PostDataItemModel> Data { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public int Draw { get; set; }
        public string Error { get; set; }
    }

    public class PostDataItemModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Posted { get; set; }
        public string Modified { get; set; }
        public bool Published { get; set; }
        public int Comments { get; set; }
    }
}
