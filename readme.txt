SimpleBlog application implementation

Description:
SimpleBlog is an application to create and manage your own blog posts.
You've got cabinet to create or edit your post, to manage comments and tags
to posts. On the front side this is a simple blog list where someone can left
replay to post or comment.

Technical details:
ASP.NET CORE 2.0, .Net framework 4.6.1, jQuery 3.2.1, Bootstrap 4.0.0-beta,
Font Awesome, gulp and other small libs and components.

How to start project
1. Make sure you have Microsoft Visual Studio 2017 (any version) with latest
updates. NET Core 2.0.0 SDK should be installed. For more details visit
https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc?tabs=aspnetcore2x

2. In solution explorer open appsettings.json file.
- Edit "ConnectionStrings" section by entering your SQL server connection string.
- Edit "AppConfiguration" as you wish. For example, change author,
  application name. It will be displayed in some views.
- Edit "BlogConfiguration" according to you needs.
- Important!!! Change reCAPTCHAKey.
  Visit https://developers.google.com/recaptcha/intro for more detail.

3. When right ConnectionStrings entered, you should create database scheme.
It's pretty simple. In Visual Studio open package management console window
and execute update-database command. This will create database according to
project's migrations classes. Now project ready to start. But database is still
empty so that after start you've got empty result page.
You should register before. There are no register links in UI but it still
works. Just add /account/register to you address, fill inputs and you will be
able to create, manage post, tags and comments.

BETTER WAY!!! (Optional)
4. To start application with random test data, after creating database by executing
update-database command, open your SQL server query editor and copy/past script
from /Seed/seed.sql file into query editor. Execute query. After success added
data you can ran SimpleBlog and you will see blog list with test data and
comments.
In this way:
    login:      admin@simple.blog
    password:   Admin18*
Use it to manage test data or enter new one.
Be sure you still can create new account as it was mentioned, in p.3 and work with
test data.

THATS IT!
Best regards, Oleg


